const {Router} = require('express');
const {validationResult} = require('express-validator');
const Post = require('../../models/post');
const auth = require('../../middleware/auth');
const file = require('../../middleware/file');
const Application = require('../../models/application');
const {postValidators} = require('../../utils/validators');

const router = Router();

router.get('/', auth, async (req, res) => {
    const posts = await Post.find();
    const applications = await Application.find();
    res.render('admin', {
        title: 'Управление сайтом',
        isPosts: true,
        posts,
        applications
    })
})

router.get('/addpost', auth, (req, res) => {
    res.render('addPost', {
        title: 'Добавить статью',
        isAddPost: true
    })
})

router.post('/addpost', auth, postValidators, async (req, res) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
        return res.status(422).render('addPost', {
            title: 'Добавить статью',
            isAddPost: true,
            error: errors.array()[0].msg,
            data: {
                title: req.body.title,
                description: req.body.description,
                img: req.body.img,
            }
        })
    }
    
    function getToday() {
      let today = new Date();
      let dd = String(today.getDate()).padStart(2, "0");
      let mm = String(today.getMonth() + 1).padStart(2, "0");
      let yyyy = today.getFullYear();
      return `${dd}/${mm}/${yyyy}`;
    }
    console.log(req.file);
    
    const post = new Post({
        title: req.body.title,
        description: req.body.description,
        img: req.file.path,
        date: getToday().toString()
    })
    
    try {
        await post.save();
        res.redirect('/posts');
    } catch (e) {
        console.log(e);
    }
})

router.get('/post/:id/edit', auth, async (req, res) => {
    if(!req.query.allow) {
        return res.redirect('/')
    }

    const post = await Post.findById(req.params.id);

    res.render('post-edit', {
        title: `Редактировать ${post.title}`,
        post
    })
})

router.post('/post/remove', auth, async (req, res) => {
    try {
        await Post.deleteOne({_id: req.body.id});
        res.redirect('/admin#posts')
    } catch (e) {
        console.log(e);
    }
    

})

router.post('/post/edit', auth, postValidators, async (req, res) => {
    const errors = validationResult(req);
    const {id, title, description} = req.body;
    const updatedPost = {
        title: title,
        description: description,
        img: req.file.path
    }
    console.log(updatedPost);
    if(!errors.isEmpty()) {
        return res.status(422).redirect(`/posts/${id}/edit?allow=true`)
    }

    delete req.body.id;
    await Post.findByIdAndUpdate(id, updatedPost);
    res.redirect('/admin/#posts')
})

router.get('/post/:id', async (req, res) => {
    const post = await Post.findById(req.params.id)

    res.render('post', {
        title: post.title, 
        post
    });
})

router.get('/application/:id/edit', auth, async (req, res) => {
    if(!req.query.allow) {
        return res.redirect('/')
    }

    const application = await Application.findById(req.params.id);

    res.render('application-edit', {
        title: `Редактировать ${application.title}`,
        application
    })
})

router.post('/application/edit', auth, async (req, res) => {
    const {id} = req.body;
    delete req.body.id;
    await Application.findByIdAndUpdate(id, req.body);
    res.redirect('/admin/#applications')
})

router.get('/application/:id', auth, async (req, res) => {
    const application = await Application.getById(req.params.id)

    res.render('application', {
        title: application.title, 
        application
    });
})


module.exports = router;