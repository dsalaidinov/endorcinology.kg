const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('biopsy', {
        title: 'Биопсия узлов щитовидной железы',
        isServices: true
    });
});

module.exports = router;