const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('pompa', {
        title: 'Установка инсулиновой помпы',
        isServices: true
    });
});

module.exports = router;