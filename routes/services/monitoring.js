const {Router} = require('express');
const router = Router();

router.get('/', (req, res) => {
    res.render('monitoring', {
        title: 'Система непрерывного мониторинга глюкозы крови',
        isServices: true
    });
});

module.exports = router;